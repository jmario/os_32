#ifndef KB_H
#define	KB_H

#ifdef	__cplusplus
extern "C" {
#endif

#define BUFFER_SIZE 128
unsigned char BUFFER[BUFFER_SIZE];


unsigned CAPS = 0;
unsigned SHIFT = 0;
unsigned actualPos = 0;
unsigned writePos = 0;

unsigned char ReadKey()
{      
    unsigned char character = BUFFER[actualPos];
    BUFFER[actualPos] = 0x0;
    if(character!=0)
    return character;         
}

void CleanBuffer(char* str, unsigned length){
    
    for(int i = 0; i < length; i++)
        str[i] = 0x00;
    
}

void BackSpace(){
    set_cursor(get_cursor()-1);
    puts(" ");
    set_cursor(get_cursor()-1);
}

void MoveDirectionKeys(unsigned char scancode){
    if(scancode == 75)
        set_cursor(get_cursor() - 1);
    else if(scancode == 77)
        set_cursor(get_cursor() + 2);
}

unsigned char kbdus[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '+', '\b',	/* Backspace */
  0,			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '{', '}', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   0,		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/', 0,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};

unsigned char kbdusLOCK[128] =
{
    0,  27, '!', 0, '#', '$', '%', 0, '&', '*',	/* 9 */
  '(', ')', '_', '=', '\b',	/* Backspace */
  0,			/* Tab */
  'Q', 'W', 'E', 'R',	/* 19 */
  'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';',	/* 39 */
 '"', '`',   0,		/* Left shift */
 '\\', 'Z', 'X', 'C', 'V', 'B', 'N',			/* 49 */
  'M', ':', '>', '?', 0,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};

/* Handles the keyboard interrupt */
void keyboard_handler(struct regs *r)
{
    unsigned char scancode;
    
    scancode = port_byte_in(0x60);

    if (scancode & 0x80)
    {
        if(scancode == 186 && !CAPS)
            CAPS = 1;
        else if(scancode == 186 && CAPS)
            CAPS = 0;
        
        if(scancode == 170 || scancode == 182)
            SHIFT = 0;    
    }
    else
    {
        
        if(actualPos < BUFFER_SIZE)
            actualPos++;
        else
            actualPos = 0;
        
        if(scancode == 42 || scancode == 54)
            SHIFT = 1;
        
        MoveDirectionKeys(scancode);
        
        if(kbdus[scancode] != 0)
            if(CAPS && !SHIFT)
                BUFFER[actualPos] = kbdusLOCK[scancode];
            else if(SHIFT && !CAPS)
                BUFFER[actualPos] = kbdusLOCK[scancode];
            else
                BUFFER[actualPos] = kbdus[scancode];
            
    }
       
}

void ReadString(char* str, unsigned length){
	int i = 0;
	char current = 0;
	CleanBuffer(BUFFER, BUFFER_SIZE);
        CleanBuffer(str, length);
        
	while( current != '\n' ){
                cli();
                current = ReadKey();
                sti();
                
                if(actualPos == length){
                    current = '\n';
                    break;
                }
                
                if(current != 0)
                    if(current == '\t'){
                        set_cursor(get_cursor()+4);
                        
                    }else if(current == '\b' && i > 0){
                            str[i] = 0x00;
                            BackSpace();
                            i--;	
                            
                    }else if(i < length-1 && current != '\b' && current != '\n'){
                            str[i] = current;
                            kprintf("%c", str[i]);
                            i++;
                    }  
	}
}

void PrintString(const char* buffer){

        cputs(buffer, 0x7);
}

void PrintColoredString(const char* buffer, unsigned color){

        cputs(buffer, color);
}

#ifdef	__cplusplus
}
#endif

#endif	/* KB_H */