#include "system.h"

void Interrupt128(struct regs *r){
    switch((unsigned)r->eax){
        case 0: 
            ReadString((char*)r->ebx, (unsigned)r->ecx);
            break;
        case 1:
            PrintString((char*)r->ebx);
            break;
        case 2: 
            PrintColoredString((char*)r->ebx, (unsigned)r->ecx);
            break;
        case 3: 
            clear_screen();
            break;  
        case 4: 
            listPartitions();
            break;
        case 5:
            r->eax = setDirClusterByName((char*)r->ebx, (unsigned)r->ecx);
            break;
        case 6:
            ls_dir((unsigned)r->ebx);
            break;
        case 7:
            readFile((unsigned)r->ebx, (char*)r->ecx);
            break;
        case 8: createDir((unsigned)r->ebx,(char*)r->ecx);
            break;
        case 9: createFile((unsigned)r->ebx,(char*)r->ecx);
            break;
        case 10: delete((unsigned)r->ebx, (char*)r->ecx);
            break;
    }
}
