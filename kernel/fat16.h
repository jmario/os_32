/* 
 * File:   fat16.h
 * Author: jmibarra
 *
 * Created on September 22, 2015, 9:53 AM
 */

#include "types.h"

#ifndef FAT16_H
#define	FAT16_H

#define FAT_FILE_LEN 8

typedef struct dir_entry{
    uint8_t DIR_Name[11]; //extension bits 8 to 10 
    uint8_t DIR_Attr;
    uint8_t DIR_NTEtr;
    uint8_t DIR_CrtTimeTenth;
    uint16_t DIR_CrtTime;
    uint16_t DIR_CrtDate;
    uint16_t DIR_LstAccDate;
    uint16_t DIR_FstClusHI;
    uint16_t DIR_WrtTime;
    uint16_t DIR_WrtDate;
    uint16_t DIR_FstClusLO;
    uint32_t DIR_FileSize;
}__attribute__((packed)) dir_entry_t;

//boot partition record
typedef struct fat_bpr
{
        uint8_t jmpBoot[3]; // boot jump code
	uint8_t oem[FAT_FILE_LEN]; // OEM name Default: “MSWIN4.1”
        uint16_t bytes_sector; // bytes per sector
	uint8_t sectors_cluster; // sectors per cluster
	uint16_t reserved_sectors; // reserved sectors
	uint8_t numFats; // number of FATs
	uint16_t root_entries; // root directory entries
	uint16_t total_sectors16; // sectors in volume
	uint8_t mdt; // media descriptor type
	uint16_t sectors_fat; // sectors occupied per FAT
	uint16_t sectors_track; // sectors per track
	uint16_t heads; // number of disk heads
	uint32_t hidden_sectors; // number of hidden sectors
	uint32_t total_sectors32; // 32bit sectors in volume
	uint8_t drive_num; // drive number
	uint8_t nt_flags; // windows nt reserved flags
	uint8_t boot_signature; // boot signature
	uint32_t volume_id; // volume id serial number
	uint8_t volume_label[FAT_FILE_LEN + 3]; // volume label string
	uint8_t file_sys_type[FAT_FILE_LEN]; // fat type
        uint8_t boot[448]; // boot sector code
	uint8_t signature[2]; // boot sector signature
}__attribute__((packed)) fat_bpr_t;


//https://fossies.org/dox/TrueCrypt-7.1a-Source/structPartitionEntryMBR.html#a867486a8c9e1bebc3b53a40eac751d7f
typedef struct PartitionEntryMBR{
    uint8_t BootIndicator;
    uint8_t StartHead;
    uint8_t StartSector;
    uint8_t StartCylinder;
    uint8_t Type;
    uint8_t EndHead;
    uint8_t EndSector;
    uint8_t EndCylinder;
    uint32_t StartLBA;
    uint32_t SectorCountLBA;
}__attribute__((packed)) pembr_t;

//https://fossies.org/dox/TrueCrypt-7.1a-Source/structMBR.html
typedef struct MBR{
    uint8_t boot[446]; // boot sector code
    pembr_t partitions[4];
    uint8_t signature[2];    
}__attribute__((packed)) mbr_t;

#endif	/* FAT16_H */

