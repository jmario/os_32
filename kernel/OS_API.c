#include "system.h"

void syscall_printString(const char* buffer){

    asm ("int $0x80" :  : "a"(1), "b"(buffer):);

}

void syscall_readString(const char* buffer, unsigned size){

    asm ("int $0x80": : "a"(0), "b"(buffer), "c"(size):);

}

void syscall_printColoredString(const char* buffer, unsigned color){

    asm ("int $0x80": : "a"(2), "b"(buffer), "c"(color):);

}