#include <system.h>

#define FRAME_IN_UNITS 512 
/* minimum #units to request */
static Header base = {0};
/* empty list to get started */
static Header *freeptr = NULL;
/* start of free list */

/* free: put block ap in free list */
void free(void *ap)
{
	Header *bp = NULL, *p = NULL;
	bp = (Header *)ap - 1;
	/* point to block header */
	for (p = freeptr; !(bp > p && bp < p->s.ptr); p = p->s.ptr){
            if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))           
                break; /* freed block at start or end of arena */
            
        }
             
        if (bp + bp->s.size == p->s.ptr){   /* join to upper nbr */
            bp->s.size += p->s.ptr->s.size;
            bp->s.ptr = p->s.ptr->s.ptr;
                
	} else
            bp->s.ptr = p->s.ptr;
	
	if (p + p->s.size == bp) {          /* join to lower nbr */
            p->s.size += bp->s.size;
            p->s.ptr = bp->s.ptr;      
	} else
		p->s.ptr = bp;
        
        freeptr = p;
}

/* morecore: ask system for more memory */
Header *morecore(unsigned nu){
    char *cp = 0;
    Header *up = 0;
       
    int total_frames = ((nu*sizeof(Header)) + MM_BLOCK_SIZE-1)/MM_BLOCK_SIZE;
    cp = mm_alloc_frames(total_frames);
    nu = FRAME_IN_UNITS * total_frames;
    
    if (cp == (char *)0)	/* no space at all */
        return NULL;
    
    up = (Header *) cp;
    up->s.size = nu;
    free((void *)(up+1));
    return freeptr;
}

/* malloc: general-purpose storage allocator */
void *malloc(unsigned nbytes)
{
    Header *ptr = NULL, *prevptr = NULL;
    Header *morecore(unsigned);
    unsigned nunits = (nbytes+sizeof(Header)-1)/sizeof(Header) + 1;
    
    if ((prevptr = freeptr) == NULL) {
	/* no free list yet */
	base.s.ptr = freeptr = prevptr = &base;
	base.s.size = 0;
    }

    for (ptr = prevptr->s.ptr; ; prevptr = ptr, ptr = ptr->s.ptr) {
        
	if (ptr->s.size >= nunits) { /* big enough */
            if (ptr->s.size == nunits) /* exactly */
		prevptr->s.ptr = ptr->s.ptr;
            else {
                /* allocate tail end */
		ptr->s.size -= nunits;
		ptr += ptr->s.size;
		ptr->s.size = nunits;
            }

            freeptr = prevptr;
            return (void *)(ptr+1);
	}
	if (ptr == freeptr) /* wrapped around free list */
            if ((ptr = morecore(nunits)) == NULL)
                 return NULL;
               
    }
}
