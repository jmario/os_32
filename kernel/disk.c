#include "fat16.h"
#include "types.h"
#include "screen.h"

#define device 0
#define EOC 0xFFF8
#define EOF 0xFFFF
#define DIR 0x10
#define FILE 0x20

mbr_t* mbr;
fat_bpr_t* fbpr;
uint16_t* c_chain;
uint32_t rootDirSectors;
uint16_t FATSz;
uint32_t total_clusters;
uint16_t DataSec;
uint16_t ClusterCount;
uint16_t FirstDataSector;
fat_bpr_t partitions[4];
uint32_t relativeSector;
uint8_t buffer[512];
uint8_t offsetFATS;

void ReadMBR(void){
    mbr = (mbr_t*)malloc(sizeof(mbr_t));
    fbpr = (fat_bpr_t*)malloc(sizeof(fat_bpr_t));
    c_chain = (uint16_t*)malloc(sizeof(uint16_t));

    ide_read_blocks(device, 0, 1, mbr);
}

void ReadBootPartitionRecord(unsigned index){
    relativeSector = mbr->partitions[index].StartLBA;
    ide_read_blocks(device, relativeSector, 1, fbpr);
    ide_read_blocks(device, relativeSector, 4, partitions);
    initGlobals();
}


uint32_t isFAT16(void){
    return (fbpr->sectors_cluster < 65525 && fbpr->sectors_cluster > 4084) ? 1 : 0;
}

void initGlobals(void){
    if(isFAT16()){
        FATSz = fbpr->sectors_fat ? fbpr->sectors_fat : 0;
        rootDirSectors = fbpr->reserved_sectors + fbpr->numFats*FATSz +relativeSector;
        FirstDataSector = rootDirSectors + (fbpr->root_entries*32)/fbpr->bytes_sector;
        total_clusters = fbpr->sectors_fat * (fbpr->bytes_sector/fbpr->numFats);
        offsetFATS = fbpr->reserved_sectors + relativeSector;
    }
}

uint32_t getClusterFirstSector(uint32_t N){
    return N ?((N - 2) * fbpr->sectors_cluster) + FirstDataSector: rootDirSectors;
}

void ls_dir(unsigned cluster){
    uint32_t addr = getClusterFirstSector(cluster);
    dir_entry_t  currentDir[16 * fbpr->sectors_cluster];
    ide_read_blocks(device, addr, fbpr->sectors_cluster, currentDir);
    
    cputs("\t\nName            Type\n", 13);
    for(int i = 0; i < 16 * fbpr->sectors_cluster; i++)
        if(currentDir[i].DIR_Attr == DIR && currentDir[i].DIR_Name[0] != 0xE5 && currentDir[i].DIR_Name[0] != 0){
            kprintf("\t%s     dir\n", currentDir[i].DIR_Name);  
        }else if(currentDir[i].DIR_Attr == FILE && currentDir[i].DIR_Name[0] != 0xE5 && currentDir[i].DIR_Name[0] != 0){
           kprintf("\t%s     file\n", currentDir[i].DIR_Name);  

        }   
}

uint32_t str_cmp(char* str, char* str2){
   
    for(int i = 0; i < 8 && str2[i] != ' ' ; i++)
        if(str[i] != str2[i])
            return 0;
         
    return 1;
                
}
uint32_t temp_cluster = 0; 
uint32_t setDirClusterByName(char* name, uint32_t cluster){
    uint32_t addr = getClusterFirstSector(cluster);
    dir_entry_t  currentDir[16 * fbpr->sectors_cluster];
    ide_read_blocks(device, addr, fbpr->sectors_cluster, currentDir);
    int found = 0;
    for(int i = 0; i < 16 * fbpr->sectors_cluster; i++){
        if(currentDir[i].DIR_Attr == DIR){
            if(str_cmp(name, currentDir[i].DIR_Name) == 1){
                temp_cluster = currentDir[i].DIR_FstClusLO;
                found = 1;
            }     
        }            
    }
    
    if(!found)
        cputs("\n\tNo existent dir", 4);
    
    return temp_cluster;
}    

void listPartitions(void){
    
    for(int i = 0; i < 4; i++){
        cputs("\n\tPartition #", YELLOW);        kprintf(" %u", i);
        if(partitions[i].bytes_sector == 512 || partitions[i].bytes_sector == 1024 ||
           partitions[i].bytes_sector == 2048 || partitions[i].bytes_sector == 4096){
            puts("\n\t\tStatus:\t");      cputs("Present", CYAN);
            kprintf("\n\t\tOEM:\t"); puts(partitions[i].oem);
            kprintf("\n\t\t# of Sectors:\t%u", partitions[i].sectors_fat);
            kprintf("\n\t\t# of Clusters:\t%u", partitions[i].sectors_cluster);
        }else{
            cputs("\n\t\tStatus:\t", WHITE); cputs("Not Present\n", RED); 
        }    
    }
}

uint32_t temp_cluster2 = 0; 
uint32_t setFileClusterByName(uint32_t cluster, char* name){
    uint32_t addr = getClusterFirstSector(cluster);
    dir_entry_t  currentDir[16 * fbpr->sectors_cluster];
    ide_read_blocks(device, addr, fbpr->sectors_cluster, currentDir);
    int found = 0;
    for(int i = 0; i < 16 * fbpr->sectors_cluster; i++){
        if(currentDir[i].DIR_Attr == FILE){
            if(str_cmp(name, currentDir[i].DIR_Name) == 1){
                temp_cluster2 = currentDir[i].DIR_FstClusLO;
                found = 1;
            }     
        }            
    }
    
    if(!found){
        cputs("\n\tNo existent file", 4);
        return 0;
    }
    return temp_cluster2;
}

void readFile(uint32_t dir_cluster, char* filename){

    uint8_t* content;
    content = (uint8_t*)(sizeof(uint8_t));
    uint32_t file_cluster = setFileClusterByName(dir_cluster, filename);

    uint32_t file_sector = getClusterFirstSector(file_cluster);
    ide_read_blocks(device, offsetFATS, fbpr->sectors_fat, c_chain);

    if(file_cluster){
        ide_read_blocks(device, file_sector, fbpr->sectors_cluster, content);
        cputs("\n Content:\t", LIGHT_RED);
        puts(content);
        file_cluster = c_chain[file_cluster];
    }
}

uint16_t getFreeCluster(){
    uint16_t free_c;
    ide_read_blocks(device, offsetFATS, fbpr->sectors_fat, c_chain);

    for(uint16_t i = free_c; i < total_clusters; i++){
            if(c_chain[i] == 0x00){
                free_c = i;
                c_chain[i] = EOF;
                ide_write_blocks(device, offsetFATS, fbpr->sectors_fat, c_chain);
                return free_c;
            }
    }
}

void delete(uint32_t cluster, char* filename){
   
    uint32_t offset = getClusterFirstSector(cluster);
    dir_entry_t file_entry[64];
    uint32_t current_cluster;
    
    ide_read_blocks(device, offset, 4, file_entry);

    for(unsigned int i = 0; i < 64; i++){
    	if(str_cmp(filename, file_entry[i].DIR_Name) == 1){
            current_cluster = file_entry[i].DIR_FstClusLO;
            file_entry[i].DIR_Name[0] = 0xE5;
            ide_write_blocks(device, offset, 4, file_entry);
            ide_read_blocks(device, offsetFATS, fbpr->sectors_fat, c_chain);
            c_chain[current_cluster] = 0x00;
            ide_write_blocks(device, offsetFATS, fbpr->sectors_fat, c_chain);
            break;
        }
    } 
}

void createFile(uint32_t dir, char* filename){
    
    uint16_t free_c = getFreeCluster();
    dir_entry_t newfile;
    dir_entry_t newEntry[64];
    for(uint16_t i = 0; i < 8; i++)
        newfile.DIR_Name[i] = filename[i];
    
    newfile.DIR_Name[8] = 'T';
    newfile.DIR_Name[9] = 'X';
    newfile.DIR_Name[10] = 'T';
    newfile.DIR_Attr = FILE;
    newfile.DIR_FstClusLO = free_c;
    newfile.DIR_FileSize = 512;

    uint32_t offset = getClusterFirstSector(dir);
    uint32_t free_offset =  getClusterFirstSector(free_c);
    
    ide_read_blocks(device, offset, 4, newEntry);

    for(uint16_t i = 0; i < 64; i++){
    	if(newEntry[i].DIR_Name[0]== 0 || newEntry[i].DIR_Name[0]== 0xE5){
            newEntry[i] = newfile;
            ide_write_blocks(device, offset, 4, newEntry); 
            puts("\n    Enter text >> ");
            ReadString(buffer, sizeof(buffer));
            ide_write_blocks(device, free_offset, 1, buffer);
            return;
    	}
    }

   
}

void createDir(uint32_t cluster, char* filename){
    
    uint16_t free_c = getFreeCluster();   
    dir_entry_t newDir;
    dir_entry_t dir_same;
    dir_entry_t dir_parent;
    dir_entry_t dirEntry[64];
    
    for(uint16_t i = 0; i < 7; i++)
        newDir.DIR_Name[i] = filename[i];
    
    newDir.DIR_Attr = DIR; 
    newDir.DIR_FileSize = 512;
    newDir.DIR_FstClusLO = free_c;

    uint32_t offset_toWrite = getClusterFirstSector(cluster);
    uint32_t dir_parent_offset =  getClusterFirstSector(free_c);
    ide_read_blocks(device, offset_toWrite, 4, dirEntry);

    for(unsigned int i = 0; i < 64; i++){
    	if(dirEntry[i].DIR_Name[0] == 0 || dirEntry[i].DIR_Name[0] == 0xE5){
	    dirEntry[i] = newDir;
            ide_write_blocks(device, offset_toWrite, 4, dirEntry);
            
            dir_same.DIR_Name[0] = '.'; dir_same.DIR_Name[1] = 0;
            dir_same.DIR_Name[8] = 0;
            dir_same.DIR_Attr = DIR;
            dir_same.DIR_FstClusLO = free_c;
            dir_same.DIR_FileSize = 0xFF;

            dir_parent.DIR_Name[0] = '.'; dir_parent.DIR_Name[1] = '.'; dir_parent.DIR_Name[2] = 0;
            dir_parent.DIR_Name[8] = 0;
            dir_parent.DIR_Attr = DIR;
            dir_parent.DIR_FstClusLO = cluster;
            dir_parent.DIR_FileSize = 0xFF;

            dirEntry[0] = dir_same;    dirEntry[1] = dir_parent;
            ide_write_blocks(device, dir_parent_offset, 4, dirEntry);
    	}
    }     
}