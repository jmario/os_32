#include <system.h>
#include <screen.h>
#include <low_level.h>
#include "kb.h"
#include "fat16.h"

void main(struct multiboot_info *mbinfo, uint32_t kernel_end_addr)
{   
    gdt_init();
    idt_init();
    irq_init();
    isrs_init();    
    memory_manager_init(mbinfo, kernel_end_addr);   
    sti();
    irq_install_handler(1, keyboard_handler);  
    init_ide_devices();
    clear_screen();
    cputs("Free OS", WHITE);
    ReadMBR();
    ReadBootPartitionRecord(0);
    typedef void (*call_module_t)(unsigned);
  
    if(mbinfo->mods_count > 0){
        unsigned int address_of_module = (unsigned)mbinfo->mods_addr[0].mod_start;
        call_module_t start_program = (call_module_t) address_of_module;
        start_program(address_of_module);
    }
      
    while(1);
}